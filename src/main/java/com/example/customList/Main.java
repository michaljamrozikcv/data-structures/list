package com.example.customList;

import com.example.customList.elements.Car;
import com.example.customList.elements.Person;

public class Main {
    public static void main(String[] args) {
        listOfPeople();
        listOfCars();
    }

    private static void listOfCars() {
        System.out.println("\n=========================================================================");
        System.out.println("LIST OF CARS");
        System.out.println("=========================================================================");
        Car c1 = new Car("model1", "vin1");
        Car c2 = new Car("model2", "vin2");
        Car c3 = new Car("model3", "vin3");
        Car c4 = new Car("model4", "vin4");
        Car c5 = new Car("model5", "vin5");
        Car c6 = new Car("modelNEW", "vinNEW");
        Car c7 = new Car("modelREPLACE", "vinREPLACE");
        Car c8 = new Car("modelREPLACE2", "vinREPLACE2");

        CustomList<Car> list = new CustomList<>();
        System.out.println("\n----------------------Adding to list at the end-------------------");
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        System.out.println(list);
        int index = 1;
        System.out.println("\n----------------------Get item on index " + index + " -------------------");
        System.out.println(list.get(index));
        System.out.println("\n----------------------Adding to list on the specific index-------------------");
        list.add(2, c6);
        System.out.println(list);
        System.out.println("\n------------------------Removing by index------------------------");
        System.out.println("item removed: " + list.remove(0));
        System.out.println(list);
        System.out.println("\n------------------------Removing by item------------------------");
        System.out.println("Does element " + c4 + " has been removed? " + list.remove(c4));
        System.out.println(list);
        System.out.println("\n------------------------Set/replace by index------------------------");
        list.set(0, c7);
        System.out.println(list);
        System.out.println("\n------------------------Set/replace by item------------------------");
        list.set(c5, c8);
        System.out.println(list);
        System.out.println("\n-----------------------Contains-------------------------------");
        System.out.println("Does queue contains: " + c3 + "--> " + list.contains(c3));
        System.out.println("Does queue contains: " + c2 + "--> " + list.contains(c2));
    }

    private static void listOfPeople() {
        System.out.println("\n=========================================================================");
        System.out.println("LIST OF PEOPLE");
        System.out.println("=========================================================================");
        Person p1 = new Person("name1", "surname1");
        Person p2 = new Person("name2", "surname2");
        Person p3 = new Person("name3", "surname3");
        Person p4 = new Person("name4", "surname4");
        Person p5 = new Person("name5", "surname5");
        Person p6 = new Person("nameNEW", "surnameNEW");
        Person p7 = new Person("nameREPLACE", "surnameREPLACE");
        Person p8 = new Person("nameREPLACE2", "surnameREPLACE2");
        CustomList<Person> list = new CustomList<>();
        System.out.println("\n----------------------Adding to list at the end-------------------");
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);
        list.add(p5);
        System.out.println(list);
        int index = 1;
        System.out.println("\n----------------------Get item on index " + index + " -------------------");
        System.out.println(list.get(index));
        System.out.println("\n----------------------Adding to list on the specific index-------------------");
        list.add(2, p6);
        System.out.println(list);
        System.out.println("\n------------------------Removing by index------------------------");
        System.out.println("item removed: " + list.remove(0));
        System.out.println(list);
        System.out.println("\n------------------------Removing by item------------------------");
        System.out.println("Does element " + p4 + " has been removed? " + list.remove(p4));
        System.out.println(list);
        System.out.println("\n------------------------Set/replace by index------------------------");
        list.set(0, p7);
        System.out.println(list);
        System.out.println("\n------------------------Set/replace by item------------------------");
        list.set(p5, p8);
        System.out.println(list);
        System.out.println("\n-----------------------Contains-------------------------------");
        System.out.println("Does queue contains: " + p3 + "--> " + list.contains(p3));
        System.out.println("Does queue contains: " + p2 + "--> " + list.contains(p2));
    }
}