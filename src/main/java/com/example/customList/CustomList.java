package com.example.customList;

public class CustomList<T> {
    private int size;
    private Node last;

    /**
     * Get object from given index
     *
     * @param index list's index
     */
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        }
        Node temp = last;
        int tempIndex = size - 1;
        while (tempIndex != index) {
            temp = temp.previous;
            tempIndex--;
        }
        return temp.item;
    }

    /**
     * Add object at the end of a list
     *
     * @param item object to be added
     */
    public void add(T item) {
        Node node = new Node(item);
        node.previous = last;
        last = node;
        size++;
    }

    /**
     * Add object at the specific list's index
     *
     * @param index number of index where object should be added
     * @param item  object to be added
     */
    public void add(int index, T item) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        } else {
            // add on existing indexes range
            int tempIndex = size - 1;
            Node temp = last;
            while (tempIndex != index) {
                tempIndex--;
                temp = temp.previous;
            }
            Node node = new Node(item);
            node.previous = temp.previous;
            temp.previous = node;
            size++;
        }
    }

    /**
     * Remove object from specific index
     *
     * @param index list's index from which user wants to remove object
     */
    public T remove(int index) {
        Node removed = last;
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        } else if (index == size - 1) {
            last = last.previous;
            size--;
        } else {
            int tempIndex = size - 2;
            Node temp = last;
            while (tempIndex != index) {
                tempIndex--;
                temp = temp.previous;
            }
            size--;
            removed = temp.previous;
            temp.previous = temp.previous.previous;
        }
        return removed.item;
    }

    /**
     * Remove specific object from list
     *
     * @param item object we want to remove from a list
     */
    public boolean remove(T item) {
        if (size == 0) {
            return false;
        }
        if (last.item == item) {
            last = last.previous;
            size--;
            return true;
        } else {
            int tempIndex = size - 2;
            Node temp = last;
            while (temp.previous != null) {
                if (temp.previous.item == item) {
                    temp.previous = temp.previous.previous;
                    size--;
                    return true;
                }
                temp = temp.previous;
                tempIndex--;
            }
        }
        return false;
    }

    /**
     * Replace item on specific index by new item
     *
     * @param index list's index where we want to replace current object
     * @param item  object we want to put on specific index
     */
    public void set(int index, T item) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        } else {
            if (index == size - 1) {
                Node node = new Node(item);
                node.previous = last.previous;
                last = node;
            } else {
                int tempIndex = size - 2;
                Node temp = last;
                while (tempIndex != index) {
                    tempIndex--;
                    temp = temp.previous;
                }
                Node node = new Node(item);
                node.previous = temp.previous.previous;
                temp.previous = node;
            }
        }
    }

    /**
     * Replace item on specific index by new item
     *
     * @param itemToReplace object in list to be replaced
     * @param newItem       new object to be put into list
     */
    public void set(T itemToReplace, T newItem) {
        if (last.item == itemToReplace) {
            Node node = new Node(newItem);
            node.previous = last.previous;
            last = node;
        } else {
            Node temp = last;
            while (temp.previous != null) {
                if (temp.previous.item == itemToReplace) {
                    Node node = new Node(newItem);
                    node.previous = temp.previous.previous;
                    temp.previous = node;
                }
                temp = temp.previous;
            }
        }
    }

    /**
     * Check if specific object is already on a list
     *
     * @param item object to be checked if already added to the list
     */
    public boolean contains(T item) {
        Node temp = last;
        while (temp != null) {
            if (temp.item == item) {
                return true;
            }
            temp = temp.previous;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("List [");
        if (last == null) {
            return result.append("] size: ").append(" size: ").append(size).toString();
        } else {
            Node temp = last;
            while (temp.previous != null) {
                result.append(temp).append(" --> ");
                temp = temp.previous;
            }
            result.append(temp);
        }
        return result + "]" + " size: " + size;
    }

    private class Node {
        private final T item;
        private Node previous;

        private Node(T item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return item.toString();
        }
    }
}
